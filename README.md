# README #

1. Ensure you have Nodejs installed. (https://nodejs.org/en/)
2. Ensure you have the Ionic CLI installed. (npm install -g @ionic/cli)
3. CD into the Ionic-Login folder.
4. Run "npm install".
5. Run "ionic serve".